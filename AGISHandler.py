# Interface to AGIS data. Used for getting site and blacklisting information.

from datetime import datetime
import json
import logging
import re
import requests

class AGISException(Exception):
    pass

logger = logging.getLogger("AGISHandler")

def _getAGISInfo(url, cache):
    '''
    Try to download json from the url and save to cache. If download fails use
    previously cached data. If that fails raise AGISException. Returns a
    dictionary of the parsed json
    '''
    try:
        data = requests.get(url).json()
    except Exception as e:
        logger.warn("Failed to get AGIS info from %s: %s", url, str(e))
        try:
            with open(cache) as data_file:
                data = json.load(data_file)
            logger.info("Loaded AGIS info from local cache")
        except Exception as e:
            raise AGISException("Failed to get blacklisting information from AGIS or local cache")
    else:
        # dump info to cache
        with open(cache, 'w') as data_file:
            json.dump(data, data_file)
            
    return data

def _filterAGISInfo(data, sites, token):
    '''
    Do the filtering of data
    '''
    for site in sites[:]:
        agissite = site + '_' + token
        # Expand Tx
        if re.match('T[0123]$', site):
            tiersites = [s['name'] for s in data if re.search('_%s$' % token, s['name']) and \
                                                    s['tier_level'] == int(site[1]) and \
                                                    s['state'] == 'ACTIVE' and not \
                                                    re.search('MWTEST|RUCIOTEST', s['name'])]
            tiersites = [re.sub('_%s$' % token, '', s) for s in tiersites]
            logger.info("Expanding %s to %s", site, tiersites)
            sites.remove(site)
            sites.extend(tiersites)
            continue
        # Remove non-existing sites
        if agissite not in [s['name'] for s in data]:
            logger.warning('%s does not exist in AGIS, will not use', agissite)
            sites.remove(site)
            continue
        # Remove disabled sites
        if agissite in [s['name'] for s in data if s['state'] == 'DISABLED']:
            logger.warning('%s is DISABLED in AGIS, will not use', agissite)
            sites.remove(site)

def getDDMEndpoints(cachedir, srcsites, destsites, srctoken, desttoken):
    '''
    Take the lists of sources and destinations sites, expand groupings like T1
    and remove disabled or non-existing sites.
    '''
    
    url = "http://atlas-agis-api.cern.ch/request/ddmendpoint/query/list/?json"
    cache = "/".join([cachedir, "agis-ddmendpoint.data"])
    data = _getAGISInfo(url, cache)
    
    _filterAGISInfo(data, srcsites, srctoken)
    _filterAGISInfo(data, destsites, desttoken)

    
def getBlacklistedEndpoints(cachedir):
    '''
    Get the list of currently blacklisted endpoints from AGIS. Returns a tuple
    of ([blacklisted reading], [blacklistwriting]). Caches data in the specified
    cache dir and uses this if AGIS is unavailable.
    '''
    
    url = "http://atlas-agis-api.cern.ch/request/ddmendpointstatus/query/list/?json"
    cache = "/".join([cachedir, "agis-blacklisting.data"])
    data = _getAGISInfo(url, cache)
    
    blacklistedreading = []
    blacklistedwriting = []
    
    for endpoint, operations in data.items():
        if 'r' in operations and operations['r']['status']['value'] == 'OFF':
            # check it is still valid
            exptime = operations['r']['status']['expiration']
            if not exptime or datetime.strptime(exptime, "%Y-%m-%dT%H:%M:%S") > datetime.utcnow():
                blacklistedreading.append(endpoint)
        if 'w' in operations and operations['w']['status']['value'] == 'OFF':
            exptime = operations['w']['status']['expiration']
            if not exptime or datetime.strptime(exptime, "%Y-%m-%dT%H:%M:%S") > datetime.utcnow():
                blacklistedwriting.append(endpoint)
    
    logger.info("AGIS endpoints blacklisted for read: %s", blacklistedreading)
    logger.info("AGIS endpoints blacklisted for write: %s", blacklistedwriting)
    return (blacklistedreading, blacklistedwriting)
